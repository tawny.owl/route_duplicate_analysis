import pandas as pd
from requests import Session
from tqdm import tqdm
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook
from operator import itemgetter
from urllib import request
from urllib3 import disable_warnings
from urllib3.exceptions import InsecureRequestWarning
import zipfile

disable_warnings(InsecureRequestWarning)

__version__ = '2.0'


class Duplicate:
    def __init__(self,
                 user,
                 password,
                 date=None,
                 url=None,
                 path=None):
        self.ldap_username = user
        self.ldap_password = password
        self.date = date if date else pd.Timestamp.now().date()
        self.url = url if url \
            else 'https://workbench.mosmetro.ru/workbench-dev/'
        self.path = path if path else ''
        self.headers = {'Content-type': 'application/json'}
        self.session = None
        if not self._check_connection():
            raise ConnectionError(
                'No connection to WB API. '
                'Check if you can connect to the internet '
                'or your WB url in params.')

        if not self._analyze():
            raise ConnectionError('Wrong user/password combination')

    def change_date(self, date):
        self.date = date
        if not self._check_connection():
            raise ConnectionError(
                'No connection to WB API. '
                'Check if you can connect to the internet '
                'or your WB url in params.')

        if not self._analyze():
            raise ConnectionError('Wrong user/password combination')

    def _analyze(self):
        with Session() as session:
            self.session = session
            if not self._authentication(self.ldap_username, self.ldap_password):
                return False
            else:
                print('GET API-QUERY: trip stops')
                trip_stops_df = pd.DataFrame(
                    [(key, var) for (key, L) in
                     self._get_trip_stops().items() for var in L],
                    columns=['trip_id', 'stop_id'])
                print('GET API-QUERY: stops')
                stop_sites = pd.DataFrame(self._get_stop_sites())[
                    ['id', 'siteId']]
                stop_sites = stop_sites.rename(columns={'id': 'stop_id',
                                                        'siteId': 'site_id'})
                trip_sites_df = trip_stops_df.merge(
                    stop_sites, on='stop_id', how='left')[
                    ['trip_id', 'site_id']]
                trip_sites_df.dropna(inplace=True, how='any')
                trip_sites = {idx: list(map(int, group['site_id'].tolist()))
                              for idx, group in
                              trip_sites_df.groupby('trip_id')}
                trip_sites = self._drop_stop_duplicates(trip_sites)
                print('GET API-QUERY: mvns')
                trip_routes = pd.DataFrame(self._get_mvns())\
                    .set_index('id').to_dict()
                print('GET API-QUERY: tram mvns')
                trip_tm_routes = pd.DataFrame(
                    self._get_mvns(True))\
                    .set_index('id').to_dict()

                wrong_trips = []
                for t in trip_sites.keys():
                    if int(t) not in trip_routes['routeId'].keys():
                        wrong_trips.append(t)

                for d in wrong_trips:
                    del trip_sites[d]

                links = dict()
                trip_links = dict()
                for trip_id in tqdm(trip_sites.keys()):
                    trip_links[trip_id] = dict(links=[], duplicates=dict())
                    for index1 in range(len(trip_sites[trip_id]) - 1):
                        for index2 in range(index1 + 1,
                                            len(trip_sites[trip_id])):
                            if trip_sites[trip_id][index1] != \
                                    trip_sites[trip_id][index2]:
                                link = str(trip_sites[trip_id][index1])\
                                       + '_' + str(trip_sites[trip_id][index2])
                                trip_links[trip_id]['links'].append(link)
                                if link in links:
                                    if trip_id not in links[link]:
                                        links[link].append(trip_id)
                                else:
                                    links[link] = [trip_id]

                for trip in tqdm(trip_links.keys()):
                    for link in trip_links[trip]['links']:
                        for dupl in links[link]:
                            if dupl != trip:
                                if trip_routes['routeId'][int(dupl)] != \
                                        trip_routes['routeId'][int(trip)]:
                                    if dupl in trip_links[trip][
                                            'duplicates'].keys():
                                        trip_links[trip]['duplicates'][
                                            dupl] += 1
                                    else:
                                        trip_links[trip]['duplicates'][dupl] = 1
                self.links = links
                self.trip_routes = trip_routes
                self.trip_tm_routes = trip_tm_routes
                self.trip_links = trip_links
        return True

    def _authentication(self, user, password):
        data = '{"user":"' + user + '","password":"' + password + '"}'
        r = self.session.post(self.url + 'api/v1/login',
                              headers=self.headers,
                              data=data,
                              verify=False)
        if r.status_code == 200:
            api_key = r.json()['Api-Key']
            self.headers['Api-Key'] = api_key
            return True
        return False

    def _get_trip_stops(self, ids: list = None):
        params = '?' + 'date=' + str(self.date)
        if ids:
            params = '&' + '&'.join(list(map(lambda x: 'ids=' + str(x), ids)))
        return self.session.get(self.url + 'api/v1/mvns/stops' + params,
                                headers=self.headers, verify=False).json()

    def _get_stop_sites(self):
        return self.session.get(self.url + 'api/v1/stops?date='
                                + str(self.date),
                                headers=self.headers, verify=False).json()

    def _get_mvns(self, tm=False):
        params = '&transport_types=3' if tm else ''
        return self.session.get(self.url + 'api/v1/mvns?no_geometry=1&date='
                                + str(self.date)
                                + params, headers=self.headers,
                                verify=False).json()

    @staticmethod
    def _drop_stop_duplicates(converted):
        for trip in tqdm(converted.keys()):
            deleted_indexes = []
            for i, site in enumerate(converted[trip]):
                if i > 0:
                    if site == converted[trip][i - 1]:
                        deleted_indexes.append(i)
            for del_index in sorted(deleted_indexes, reverse=True):
                del converted[trip][del_index]
        return converted

    def print_link_report(self):
        wb = Workbook()
        del wb['Sheet']
        wb.create_sheet('1')
        ws1 = wb['1']
        ws1.cell(1, 1, value='site_id_from')
        ws1.cell(1, 2, value='site_id_to')
        ws1.cell(1, 3, value='Unique')
        ws1.cell(1, 4, value='Tram')
        ws1.cell(1, 5, value='sum_num_of_trips')
        ws1.cell(1, 6, value='trip_ids')
        ws1.cell(1, 7, value='tram_trip_ids')
        ws1.cell(1, 8, value='route_ids')
        ch = 2
        for i in tqdm(self.links.keys()):
            link = i.split('_')
            site_id_from = link[0]
            site_id_to = link[1]
            trip_ids = self.links[i]
            route_ids = list(set(
                [str(self.trip_routes['routeId'][int(trip)])
                 for trip in trip_ids]))
            unique = '+' if len(route_ids) == 1 else '-'
            tm_trip_ids = list(filter(
                lambda x: int(x) in self.trip_tm_routes['routeId'].keys(),
                trip_ids))
            tm = '+' if tm_trip_ids else '-'
            sum_num_of_trips = sum([self.trip_routes['numOfTrips'][int(trip)]
                                    for trip in trip_ids])
            ws1.cell(ch, 1, value=str(site_id_from))
            ws1.cell(ch, 2, value=str(site_id_to))
            ws1.cell(ch, 3, value=unique)
            ws1.cell(ch, 4, value=tm)
            ws1.cell(ch, 5, value=str(sum_num_of_trips))
            ws1.cell(ch, 6, value=','.join(trip_ids))
            ws1.cell(ch, 7, value=','.join(tm_trip_ids))
            ws1.cell(ch, 8, value=','.join(route_ids))
            ch += 1
        wb.save(self.path + 'links.csv')
        wb.close()

    def print_mvns_report(self):
        with zipfile.ZipFile(self.path + 'mvns_links.zip', 'w') as zf:
            for trip in tqdm(self.trip_links.keys()):
                wb = Workbook()
                del wb['Sheet']
                wb.create_sheet('1')
                ws1 = wb['1']
                ws1.cell(1, 1, value='trip_id')
                ws1.cell(1, 2, value='%')
                ws1.cell(1, 3, value='links count')
                ws1.cell(1, 4, value='Summary links count: ' +
                                     str(len(self.trip_links[trip]['links'])))
                ch = 2
                for dupl in sorted(self.trip_links[trip]['duplicates'].items(),
                                   key=itemgetter(1), reverse=True):
                    ws1.cell(ch, 1, value=str(dupl[0]))
                    ws1.cell(ch, 2, value=str(round(
                        self.trip_links[trip]['duplicates'][dupl[0]] /
                        len(self.trip_links[trip]['links']) * 100, 1)))
                    ws1.cell(ch, 3,
                             value=str(
                                 self.trip_links[trip]['duplicates'][dupl[0]]))
                    ch += 1
                buf = save_virtual_workbook(wb)
                zf.writestr(str(trip) + '.xlsx', buf)
                wb.close()
            zf.close()

    def _check_connection(self):
        from ssl import _create_unverified_context
        req = request.Request(self.url)
        try:
            request.urlopen(req, context=_create_unverified_context())
            return True
        except:
            return False
