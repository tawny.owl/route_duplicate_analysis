from setuptools import setup, find_packages

with open('requirements.txt') as f:
    required = f.read().splitlines()

config = {
    'name': 'duplicate_analysis',
    'description': 'Duplicate analysis library',
    'author': 'tawny.owl',
    'version': '2.0',
    'install_requires': required,
    'packages': find_packages(),
}

if __name__ == '__main__':
    setup(**config)
